let places = [{city:"Hyderabad", area:"Kukatpally", personName:"Ahmeed"}, 
{city:"Hyderabad", area:"Kukatpally", personName:"Ajmil"}, 
{city:"Hyderabad", area:"Kukatpally", personName:"aaryan"},
{city:"Banglore", area:"Kukatpally", personName:"Kiran"},
{city:"Delhi", area:"Kukatpally", personName:"Kihan"},
{city:"Hyderabad", area:"Kukatpally", personName:"XYz"},
{city:"Hyderabad", area:"Jubliee Hills", personName:"Ajmair"},
];
//places.forEach(place => console.log(place.personName));
//I want the names of persons starting with letter 'A' (case insensitive) & living in Kukatpally In Hyderabad
let nameRegx = /^a/i; //Regular expression to check for the name starting with a or A. 
places.forEach(place => {
  if(place.city == "Hyderabad" && place.area == "Kukatpally" && place.personName.match(nameRegx)){
    console.log(place);
  }
})